// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

var apiaddr = "https://api.sandbox.zorro.airtimerewards.co.uk"
var brandid = "89d54d7d-50f0-409d-962e-1147d04ced34"
var msisdn = "447456453453"

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
})

var vm = new Vue({
	el:'#balances',
	data:{
		balance: "-",
		earned_overall: "-",
		pending_balance: "-",
		redemption_threshold: "-",
	},
})


/* Get Verification code */
var data = {};
data.brand = brandid;
data.msisdn  = msisdn;
var json = JSON.stringify(data);

var xhr = new XMLHttpRequest();
xhr.open("POST", apiaddr + '/auth/mobile-login', true);
xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
xhr.onload = function () {
	var vercode = JSON.parse(xhr.responseText);
	if (xhr.readyState == 4 && xhr.status == "200") {
		var jsonreturn = JSON.parse(this.responseText);
		var vercode = jsonreturn.verification_code;
		console.log(vercode);
		verify(vercode);
	} else {
		console.error(vercode);
	}
}
xhr.send(json);


function verify(vercode) {
	console.log('runningvercode');
	var verificationcode = vercode;
	console.log(verificationcode);
    var data = {};
	data.brand = brandid;
	data.issue_permanent_token = true;
	data.msisdn  = msisdn;
	data.verification_code = verificationcode;
	var json = JSON.stringify(data);
	console.log(data);

	var xhrverify = new XMLHttpRequest();
	xhrverify.open("POST", apiaddr + '/auth/mobile-login/verify', true);
	xhrverify.setRequestHeader('Content-type','application/json;');
	xhrverify.onload = function () {
		var verdata = JSON.parse(xhrverify.responseText);
		if (xhrverify.readyState == 4 && xhrverify.status == "200") {
			console.table(verdata);
			var jsonreturn = JSON.parse(this.responseText);
			var token = jsonreturn.token;
			getbalances(token);
		} else {
			console.error(verdata);
		}
	}
	xhrverify.send(json);
}

function getbalances(token) {
	console.log('runninggetbalance');
	
	console.log(token);

	var xhrbalances = new XMLHttpRequest();
	xhrbalances.open("GET", apiaddr + '/account', true);
	xhrbalances.setRequestHeader('Authorization', 'Bearer ' + token);
	xhrbalances.setRequestHeader('Content-type','application/json;');
	xhrbalances.onload = function () {
		var balances = JSON.parse(xhrbalances.responseText);
		if (xhrbalances.readyState == 4 && xhrbalances.status == "200") {
			console.table(balances);
			var baljsonreturn = JSON.parse(this.responseText);
			vm.balance = "£" + baljsonreturn.balance.amount;
			vm.earned_overall = "£" + baljsonreturn.balance.amount;
			vm.pending_balance = "£" + baljsonreturn.balance.amount;
			vm.redemption_threshold = "£" + baljsonreturn.balance.amount;
		} else {
			console.error(balances);
		}
	}
	xhrbalances.send(json);
}






